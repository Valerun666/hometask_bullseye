//
//  ViewController.h
//  BullsEye
//
//  Created by Valerun on 19.02.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)showAlert;

@end

